+++
title = "DW84 Inc LLC Foundation"
date = "2017-05-20T12:10:51+02:00"
menu = "main"
+++

## DIGITAL INSTANTIATION 
In Programming Languages, The First Recursive Call Is To SELF.
You must build your digital identity foundation in order to participate in the digital economy.

DW84 Inc LLC Foundation Facilitation Agent primary role is communication with client.

## CRYPTOCURRENCY 

Propogate and Preserve Your Digital Assets With Blockchain Technologies.

DW84 Inc LLC Foundation Acquisition Agent primary
role is the integration of "smart contracts" with 
client digital assets.